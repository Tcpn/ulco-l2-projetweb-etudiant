<div id="product">

    <?php foreach ($params["product"] as $p) : ?>

    <div>
        <div class="product-images">
            <img id="displayImage" src="/public/images/<?=$p['image']?>">

            <div class="product-miniatures">

                <div>
                    <img class="image-miniature" src="/public/images/<?=$p['image']?>">
                </div>

                <div>
                    <img class="image-miniature" src="/public/images/<?=$p['image_alt1']?>">
                </div>
                <div>
                    <img class="image-miniature" src="/public/images/<?=$p['image_alt2']?>">
                </div>
                <div>
                    <img class="image-miniature" src="/public/images/<?=$p['image_alt3']?>">
                </div>

            </div>

        </div>

        <div class="product-infos">
            <p class="product-category">
                <?=$p['type']?>
            </p>

            <h1>
                <?=$p['name']?>
            </h1>

            <p class="product-price">
                <?=$p['price']?> €
            </p>

            <?php if ($_SESSION['id'] === null){ ?>
                <a href="/account" class="log-btn">Connectez vous pour commander un produit</a>
            <?php
            }
            else{ ?>
                <form  method="post" action="/cart/add" >
                    <button id="minus" type="button">-</button>
                    <button id="value" type="button" disabled>1</button>
                    <button id="plus" type="button">+</button>

                    <input type="hidden" name="product_Name" value="<?=$p['name']?>">
                    <input type="hidden" name="product_Image" value="<?=$p['image']?>">
                    <input type="hidden" name="product_Price" value="<?=$p['price']?>">
                    <input type="hidden" name="product_Category" value="<?=$p['type']?>">
                    <input id="product_quantity" type="hidden" name="product_Quantity" value="1" >
                    <input type="submit" value="Ajouter au pannier">
                </form>

                <div id="quantityError">

                </div>

            <?php
            }?>

        </div>

    </div>


    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?=$p['spec']?>
        </div>

        <div class="product-comments">
            <h2>Avis</h2>

            <?php foreach (\model\CommentModel::listComment($p['id']) as $product){ ?>

                <div class="product-comment">
                    <p class="product-comment-author"><?=$product['firstname']?> <?=$product['lastname']?> </p>
                    <p>
                        <?= $product['content'] ?>
                    </p>
                </div>
            <?php } ?>

            <?php if ($_SESSION['id'] === null){?>

                <p>
                    <a href="/account" class="log-btn">Connectez-vous pour poster un avis</a>
                </p>

            <?php
            }
            else {?>

            <form class="account-login" method="post" action="/postComment">
                <input type="text" name="comment" placeholder="Ajoutez un commentaire ..." />
                <input name="id_product" type="hidden" value= <?=$p['id'] ?> />
                <input type="submit" value="Envoyer" />
            </form>
            <?php } ?>


        </div>


    </div>
    <?php endforeach; ?>
</div>

<script type="text/javascript" src="/public/scripts/product.js"></script>
