<?php if ($_GET['status'] === "update_success"){?>

    <div class="box info">Vos informations personnelles ont été mises à jour !</div>
   <?php
}
else if ($_GET['status'] === "mail_error"){?>

    <div class="box error">Votre adresse mail  est déjà utilisée ou ne respecte pas le format mail</div>
<?php
}
else if ($_GET['status'] === "name_error"){?>

    <div class="box error">Votre nom et prénom doivent avoir une longueur plus grande que 2</div>
    <?php
} ?>

<?php if ($_SESSION['id'] != null){ ?>

    <div id="account_update">

        <form method="post" action="/account/update">

            <h2>Informations du compte</h2>
            <h3>Informations Personnelles</h3>

            <div>
                <p>Prénom</p>
                <input type="text" name="updatefirstname" value="<?=$_SESSION['firstname'] ?>" placeholder="Prénom" />
            </div>


            <div>
                <p>Nom</p>
                <input type="text" name="updatelastname" value="<?=$_SESSION['lastname'] ?>" placeholder="Nom" />
            </div>


            <div>
                <p>Adresse Mail</p>
                <input type="text" name="updatemail" value="<?=$_SESSION['mail'] ?>" placeholder="Adresse Mail" />
            </div>


            <input type="submit" value="Modifier mes information" />

        </form>

    </div>

    <?php
}

else { ?>
    <div id="account"> Veuillez vous <a href="/account" style=" margin-left: 5px; margin-right: 5px; color: dodgerblue"> Connecter </a>  pour modifier vos informations </div>

<?php
}

