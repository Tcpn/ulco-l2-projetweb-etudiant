<?php if ($_SESSION['cart'] == null){ ?>

    <div style="margin-left: 20px; margin-top: 40px">Votre panier est vide</div>
<?php

}else{

    $cart = $_SESSION['cart'];?>
    <h2 id="cart-text">Panier</h2>

    <div id="container">
    <div id="cart-container">
    <?php

foreach ($cart as $product){ ?>

    <div class="cart-product">

        <div class="cart-img">
            <img src="/public/images/<?= $product['image'] ?>">
        </div>


        <div class="cart-identity">
            <div class="cart-category"><?= $product['category'] ?></div>
            <div class="cart-name"><?= $product['name'] ?></div>
        </div>

        <form>
            <div class="cart-quantity">Quantité</div>
            <button class="minus" type="button">-</button>
            <button class="value" type="button"><?= $product['quantity'] ?></button>
            <button class="plus" type="button">+</button>
        </form>

        <div class="cart-priceInfos">
            <div>Prix Unitaire</div>
            <div class="cart-price"><span class="cart_price"><?= $product['price'] ?></span> €</div>
        </div>


    </div>

    <?php
}
    ?>
    </div>

        <div id="cart-update">
            <form  method="post" action="/cart/update" >
                <?php foreach ($_SESSION['cart'] as $product){ ?>
                    <input class="update_quantity" type="hidden" name="update_quantity[]" value="">
                    <?php
                } ?>
                <input id="cart-update-btn" type="hidden" value="Mettre à jour le panier">
            </form>

        </div>

<hr>
<div id="cart-TotalCheckout">
    <div id="cart-total">Total : <span id="cart-totalPrice"></span> € </div>

    <button id="cart-checkout"> <a href="/cart/payment">Proceder au paiement</a> </button>
</div>

    </div>

   <?php
} ?>

<script type="text/javascript" src="/public/scripts/cart.js"></script>



