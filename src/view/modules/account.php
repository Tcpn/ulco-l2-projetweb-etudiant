<?php if ($_GET['status'] === "logout"){?>

    <div class="box info">Déconnexion réussie ! A bientôt !</div>
   <?php
}
elseif ($_GET['status'] === "login_fail"){?>

    <div class="box error">Identifiants incorrects, veuillez réessayer.</div>
<?php
}
elseif ($_GET['status'] === "signin_success"){?>

    <div class="box info">Inscription réussie ! Vous pouvez dès à présent vous connecter !</div>
<?php
}
elseif ($_GET['status'] === "signin_error"){?>

    <div class="box error">Une erreur s'est produite, vérifiez vos information et réessayez.</div>
<?php
}?>


<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p id="signin_lastname">Nom</p>
  <input id="input_lastname" type="text" name="userlastname" placeholder="Nom" />

  <p id="signin_firstname">Prénom</p>
  <input id="input_firstname" type="text" name="userfirstname" placeholder="Prénom" />

  <p id="signin_mail">Adresse mail</p>
  <input id="input_mail" type="text" name="usermail" placeholder="Adresse mail" />

  <p id="signin_pass">Mot de passe</p>
  <input id="input_pass" type="password" name="userpass" placeholder="Mot de passe" />

  <p id="signin_pass2">Répéter le mot de passe</p>
  <input id="input_pass2" type="password" name="userpass" placeholder="Mot de passe" />

  <input id="signin_btn" type="submit" value="Inscription" />

</form>

</div>

<script type="text/javascript" src="/public/scripts/signin.js"></script>
