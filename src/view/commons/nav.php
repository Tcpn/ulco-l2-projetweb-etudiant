<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if ($_SESSION['id'] === null){?>

        <a class="account" href="/account">
            <img src="/public/images/avatar.png">
            Compte
        </a>
        <?php
    }
    else { ?>

        <a class="account" href="/account/infos">
            <img src="/public/images/avatar.png">
             <?=$_SESSION['firstname']?> <?=$_SESSION['lastname']?>
        </a>
            <a href="/cart">Panier</a>
            <a href="/account/logout">Déconnexion</a>


        <?php
    }?>

</nav>
