<?php

namespace controller;

use view\Template;


class CartController
{
    public function cart(){

        $params = array(
            "title" => "Panier",
            "module" => "cart.php",
        );

        Template::render($params);
    }


    public function add(){

        $name = $_POST['product_Name'];
        $category = $_POST['product_Category'];
        $image = $_POST['product_Image'];
        $price = $_POST['product_Price'];
        $quantity = $_POST['product_Quantity'];

        $array = array(
            "name" => $name,
            "category" => $category,
            "image" => $image,
            "price" => $price,
            "quantity"=> $quantity);

        if ($_SESSION['cart'] === null) $_SESSION['cart'] = array($array);
        else{
            $test = false;
            $cpt = 0;
            foreach ($_SESSION['cart'] as $c){
                if (strcmp($c['name'], $array['name']) === 0){
                    if ((int)$c['quantity'] + (int)$array['quantity']  <= 5) $_SESSION['cart'][$cpt]['quantity'] = (string)((int) $c['quantity'] + (int)$array['quantity'] );
                    else $_SESSION['cart'][$cpt]['quantity'] = 5;
                    $test = true;
                    break;
                }
                $cpt ++;
            }
            if (!$test) $_SESSION['cart'] = array_merge($_SESSION['cart'], array($array));
        }

        header("Location: /cart");
        exit();

    }

    public function payment(){

        $params = array(
            "title" => "Paiement",
            "module" => "payment.php",
        );

        Template::render($params);
    }

    public function update(){

        $update_quantity = $_POST['update_quantity'];

        $cart = $_SESSION['cart'];
        $cpt = 0;
        foreach ($cart as $product){

            if ((int)$product['quantity'] != $update_quantity[$cpt]){
                if ((int)$update_quantity[$cpt] === 0) unset($_SESSION['cart'][$cpt]);
                else $_SESSION['cart'][$cpt]['quantity'] = $update_quantity[$cpt];
            }
            $cpt ++;
        }

        sort($_SESSION['cart']);

        header("Location: /cart");
        exit();

    }



}