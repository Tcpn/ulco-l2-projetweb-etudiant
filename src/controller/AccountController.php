<?php

namespace controller;

use model\AccountModel;
use view\Template;

class AccountController
{
    public function account(){

        // Variables à transmettre à la vue
        $params = [
            "title"  => "Account",
            "module" => "account.php"
        ];

        Template::render($params);

    }

    public function signin(){
        $firstname = htmlspecialchars($_POST['userfirstname']);
        $lastname = htmlspecialchars($_POST['userlastname']);
        $mail = htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);

        if (AccountModel::signin($firstname, $lastname, $mail, $password)){

            header("Location: /account?status=signin_success");
        }

        else{
            header("Location: /account?status=signin_error");
        }
        exit();
    }

    public function login(){
        $mail = htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);

        $account = AccountModel::login($mail, $password);
        $account = $account[0];

        if ($account[0] != -1){

            $_SESSION['id'] = $account[0];
            $_SESSION['firstname'] = $account[1];
            $_SESSION['lastname'] = $account[2];
            $_SESSION['mail'] = $account[3];

            header("Location: /store");
        }

        else{
            header("Location: /account?status=login_fail");
        }
        exit();
    }

    public function logout(){

        session_destroy();
        header("Location: /account?status=logout");
        exit();
    }

    public function infos(){

        $params = [
            "title"  => "Account Infos",
            "module" => "infos.php"
        ];

        Template::render($params);
    }

    public function updateInfos(){

        $firstname = htmlspecialchars($_POST['updatefirstname']);
        $lastname = htmlspecialchars($_POST['updatelastname']) ;
        $mail = htmlspecialchars($_POST['updatemail']);

        $res =AccountModel::updateInfos($firstname, $lastname, $mail);

        if ($res != 1){
            $_SESSION['firstname'] = $firstname;
            $_SESSION['lastname'] = $lastname;
        }

        else{
            header("Location: /account/infos?status=name_error");
            exit();
        }


        if ($res != 2){
            $_SESSION['mail'] = $mail;
            header("Location: /account/infos?status=update_success");
        }
        else header("Location: /account/infos?status=mail_error");

        exit();
    }

}