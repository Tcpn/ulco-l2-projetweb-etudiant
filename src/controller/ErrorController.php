<?php

namespace controller;

use view\Template;

class ErrorController {

  public function error(): void
  {
    //echo "404 Not Found!";

      $params = [
          "title"  => "Error",
          "module" => "error.php"
      ];

      Template::render($params);
  }

}