<?php

namespace controller;


use model\CommentModel;

class CommentController
{
    public function postComment(): void{

        $comment = htmlspecialchars($_POST['comment']);

        $id_product = $_POST['id_product'];
        $id_account = $_SESSION['id'];

        CommentModel::insertComment($comment, $id_product , $id_account);

        header("Location: /store/$id_product?comment_success");
        exit();

    }

}