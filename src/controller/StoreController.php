<?php

namespace controller;

use model\StoreModel;
use view\Template;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = StoreModel::listCategories();

    $products = StoreModel::listProducts();

    // Variables à transmettre à la vue

      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );

    // Faire le rendu de la vue "src/view/Template.php"
    Template::render($params);
  }


  public function product(int $id): void
  {

      // Communications avec la base de données
      $product = StoreModel::infoProduct($id);

      $params = array(
          "title" => $product[0]["name"],
          "module" => "product.php",
          "product" => $product
      );

      if ($params["product"] != null){

          // Faire le rendu de la vue "src/view/Template.php"
          Template::render($params);
      }

      else{
          header("Location: /store");
          exit();
      }
  }


  public function search(): void{

     $categories = StoreModel::listCategories();
     $products = StoreModel::searchProducts();

      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );

      Template::render($params);

  }

}