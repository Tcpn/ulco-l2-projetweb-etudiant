<?php

namespace model;

class CommentModel
{

    static function insertComment($content, $id_product, $id_account): void{

        $date = date('y-m-d');
        var_dump($date);

        var_dump($date);

        $db = Model::connect();
        $sql = "INSERT into comment (content, date, id_product, id_account) VALUES (?, ?, ?, ?)";
        $req = $db->prepare($sql);
        $req->execute(array($content, $date , $id_product, $id_account ));
    }



    static function listComment($id_product): array{

        $db = Model::connect();
        $sql = "SELECT comment.content, comment.date, comment.id_product, account.firstname, account.lastname FROM comment INNER JOIN account WHERE comment.id_product = ? AND comment.id_account = account.id";
        $req = $db->prepare($sql);
        $req->execute(array($id_product));

        return $req->fetchAll();
    }

}