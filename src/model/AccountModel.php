<?php

namespace model;

class AccountModel
{
    static function check($firstname, $lastname, $mail, $password) : bool{

        if (strlen($firstname)<2) return false;
        if (strlen($lastname)<2) return false;
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) return false;

        $db = Model::connect();
        $sql = "SELECT account.mail FROM account";
        $request = $db->prepare($sql);
        $request->execute();
        $emails = $request->fetchAll();
        foreach ($emails as $email){ if (strcmp($email, $mail) === 0) return false;}

        if (strlen($password) < 6) return false;

        return true;
    }

    static function signin($firstname, $lastname, $mail, $password): bool{

        if (self::check($firstname, $lastname, $mail, $password) == true){

            $db = Model::connect();
            $sql = "INSERT INTO account (firstname, lastname, mail, password) VALUES (?, ?, ?, ?)";
            $req = $db->prepare($sql);
            $req->execute(array($firstname, $lastname, $mail, $password));
            return true;
        }

        else return false;

    }

    static function login($mail, $password): array{

        $db = Model::connect();
        $sql = 'SELECT account.id, account.firstname, account.lastname, account.mail, account.password FROM account WHERE account.mail = ? AND account.password = ?';
        $req = $db->prepare($sql);
        $req->execute(array($mail, $password));
        $infos = $req->fetchAll();

        if ($infos[0] != null) return $infos;
        else{
            $infos[0][0] = -1;
            return $infos;
        }

    }

    static function updateInfos($firstname, $lastname, $mail): int{

        $db = Model::connect();

        $id = $_SESSION['id'];

        if (strlen($firstname) > 2 && strlen($lastname) > 2 ){

            $sql = 'UPDATE account set firstname = ? , lastname = ? WHERE id = ? ';
            $req = $db->prepare($sql);
            $req->execute(array($firstname, $lastname,  $id));
        }

        else return 1;

        $sql = 'SELECT account.mail FROM account WHERE account.mail = ? AND account.id != ?';
        $req = $db->prepare($sql);
        $req->execute(array($mail, $id));
        $res = $req->fetchAll();

        var_dump($res);
        var_dump(sizeof($res));


        if (sizeof($res) === 0 && filter_var($mail, FILTER_VALIDATE_EMAIL)){

            $sql = 'UPDATE account set mail = ? WHERE id = ? ';
            $req = $db->prepare($sql);
            $req->execute(array($mail, $id));

            return 0;
        }
        else return 2;
    }

}