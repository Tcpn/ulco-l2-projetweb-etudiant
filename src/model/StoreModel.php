<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }



  static function listProducts(): array{

      $db = Model::connect();

      $sql = "SELECT product.id, product.name, product.price, product.image, category.name AS type, product.category FROM product INNER JOIN category WHERE category.id = product.category";

      $req = $db->prepare($sql);
      $req->execute();

      return $req->fetchAll();
  }



  static function infoProduct(int $id): array{

      $db = Model::connect();

      $sql = "SELECT product.id,  product.name, product.price, product.image,product.image_alt1, product.image_alt2, product.image_alt3,  product.spec, category.name AS type FROM product INNER JOIN category WHERE product.id = ? AND category.id = product.category";

      $req = $db->prepare($sql);
      $req->execute(array($id));

      return $req->fetchAll();
  }


    static function searchProducts(): array{

        $db = Model::connect();
        $category = $_POST['category'];
        $search = htmlspecialchars($_POST['search']);
        $order = $_POST['order'];
        $list = null;

        $cpt = 0;
        foreach ($category as $c){
            if ($cpt === 0) $list.= " WHERE ( category.name = '$c'" ;
            else  $list .=  " OR category.name = '$c'" ;
            $cpt ++;
        }
        if ($category != null) $list .= " )";

        $sql = "SELECT product.id, product.name, product.price, product.image, category.name AS type, product.category FROM product INNER JOIN category ON category.id = product.category";
        $sql .= $list;
        $sql .= " AND product.name LIKE '%$search%'";
        if ($order != null) $sql .= " ORDER BY product.price $order";

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }

}