document.addEventListener('DOMContentLoaded', function () {

    let img = document.getElementById("displayImage");
    let miniatures = document.getElementsByClassName("image-miniature");

    let value = document.getElementById("value");
    let minus = document.getElementById("minus");
    let plus = document.getElementById("plus");

    let quantity = document.getElementById("product_quantity");
    let cpt = 1;

    let error = document.getElementById("quantityError");

    for (let miniature of miniatures){
        miniature.addEventListener("click", function () {

            img.removeAttribute("src");
            img.setAttribute("src", miniature.getAttribute("src"));
        })
    }



    minus.addEventListener("click", function () {
        if (parseInt(value.innerText) > 1){
            value.innerText = parseInt(value.innerText) - 1;
            cpt = cpt - 1;
            quantity.value = cpt;
            error.innerText = "";
            error.classList.remove("box");
            error.classList.remove("error");
        }
        else if (parseInt(value.innerText) === 5){
            error.innerText = "Quantité maximale autorisée !";
            error.classList.add("error");
            error.classList.add("box");
        }
    })


    plus.addEventListener("click", function () {
        if (parseInt(value.innerText) === 5){
            error.innerText = "Quantité maximale autorisée !";
            error.classList.add("box");
            error.classList.add("error");
        }
        else if (parseInt(value.innerText) === 4) {

            value.innerText = parseInt(value.innerText) + 1;
            cpt = cpt + 1;
            quantity.value = cpt;
            error.innerText = "Quantité maximale autorisée !";
            error.classList.add("box");
            error.classList.add("error");
        }
        else{
            value.innerText = parseInt(value.innerText) + 1;
            cpt = cpt + 1;
            quantity.value = cpt;
            error.innerText = "";
            error.classList.remove("error");
            error.classList.remove("box");

        }
    })


})