let totalPrice = document.getElementById("cart-totalPrice");
let price = document.getElementsByClassName("cart_price");

let value = document.getElementsByClassName("value");
let minus = document.getElementsByClassName("minus");
let plus = document.getElementsByClassName("plus");

let productsPrice = 0

let update_quantity = document.getElementsByClassName("update_quantity");

let cart_update = document.getElementById("cart-update-btn");

let actual_quantity = [];



for (let cpt = 0; cpt < price.length; cpt ++){
    productsPrice += parseInt(price[cpt].innerText) * parseInt(value[cpt].innerText)

    if (parseInt(value[cpt].innerText) === 0) minus[cpt].disabled = true;
    else if (parseInt(value[cpt].innerText) === 5) plus[cpt].disabled = true;

    update_quantity[cpt].value = value[cpt].innerHTML;
    actual_quantity[cpt] = value[cpt].innerHTML;

}
totalPrice.innerText = productsPrice

for (let i = 0; i< value.length; i ++){

    minus[i].addEventListener("click", function () {

        if (parseInt(value[i].innerText) > 1){
            value[i].innerText = parseInt(value[i].innerText) - 1;
            plus[i].disabled = false;
            minus[i].disabled = false;
        }
        else if (parseInt(value[i].innerText) === 1){
            value[i].innerText = parseInt(value[i].innerText) - 1;
            plus[i].disabled = false;
            minus[i].disabled = true;
        }
        else {
            plus[i].disabled = false;
            minus[i].disabled = true;
        }

        productsPrice = 0
        let test = true;
        for (let cpt = 0; cpt < price.length; cpt ++){
            productsPrice += parseInt(price[cpt].innerText) * parseInt(value[cpt].innerText)
            update_quantity[cpt].value = value[cpt].innerHTML;

            if (parseInt(actual_quantity[cpt]) != parseInt(value[cpt].innerHTML) ) test = false;
        }

        totalPrice.innerText = productsPrice

        if (!test) cart_update.type = "submit";
        else cart_update.type = "hidden";

    })

    plus[i].addEventListener("click", function () {
        if (parseInt(value[i].innerText) === 5){
            plus[i].disabled = true;
            minus[i].disabled = false;
        }
        else if (parseInt(value[i].innerText) === 4) {

            value[i].innerText = parseInt(value[i].innerText) + 1;
            plus[i].disabled = true;
            minus[i].disabled = false;
        }
        else{
            value[i].innerText = parseInt(value[i].innerText) + 1;
            plus[i].disabled = false;
            minus[i].disabled = false;
        }

        cart_update.type = "submit";

        productsPrice = 0
        let test = true;
        for (let cpt = 0; cpt < price.length; cpt ++){
            productsPrice += parseInt(price[cpt].innerText) * parseInt(value[cpt].innerText)
            update_quantity[cpt].value = value[cpt].innerHTML;

            if (parseInt(actual_quantity[cpt]) != parseInt(value[cpt].innerHTML) ) test = false;
        }

        totalPrice.innerText = productsPrice;
        if (!test) cart_update.type = "submit";
        else cart_update.type = "hidden";
    })

}

