document.addEventListener('DOMContentLoaded', function () {

    let lastname = document.getElementById("signin_lastname");
    let lastname_input = document.getElementById("input_lastname");

    let firstname = document.getElementById("signin_firstname");
    let firstname_input = document.getElementById("input_firstname");

    let mail = document.getElementById("signin_mail");
    let mail_input = document.getElementById("input_mail");

    let pass = document.getElementById("signin_pass");
    let pass_input = document.getElementById("input_pass");

    let pass2 = document.getElementById("signin_pass2");
    let pass_input2 = document.getElementById("input_pass2");


    lastname_input.addEventListener("change", function (){

        if (lastname_input.value.length < 2){

            lastname.classList.remove("valid");
            lastname_input.classList.remove("valid");
            lastname.classList.add("invalid");
            lastname_input.classList.add("invalid");
        }
        else{
            lastname.classList.remove("invalid");
            lastname.classList.add("valid");
            lastname_input.classList.remove("invalid");
            lastname_input.classList.add("valid");
        }
    })


    firstname_input.addEventListener("change", function () {

        if (firstname_input.value.length < 2){

            firstname.classList.remove("valid");
            firstname_input.classList.remove("valid");
            firstname.classList.add("invalid");
            firstname_input.classList.add("invalid");
        }
        else {
            firstname.classList.remove("invalid");
            firstname_input.classList.remove("invalid");
            firstname.classList.add("valid");
            firstname_input.classList.add("valid");
        }
    })


    mail_input.addEventListener("change", function () {

        let format_mail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!format_mail.test(mail_input.value)){

            mail.classList.remove("valid");
            mail_input.classList.remove("valid");
            mail.classList.add("invalid");
            mail_input.classList.add("invalid");
        }
        else {
            mail.classList.remove("invalid");
            mail_input.classList.remove("invalid");
            mail.classList.add("valid");
            mail_input.classList.add("valid");
        }

    })


    pass_input.addEventListener("change", function () {

        if (pass_input.value.length > 6 && pass_input.value.match(( /[0-9]/g)) && ( pass_input.value.match(/[a-z]/g) || pass_input.value.match(/[A-Z]/g) ) ){

            pass.classList.remove("invalid");
            pass_input.classList.remove("invalid");
            pass.classList.add("valid");
            pass_input.classList.add("valid");
        }

        else {
            pass.classList.remove("valid");
            pass_input.classList.remove("valid");
            pass.classList.add("invalid");
            pass_input.classList.add("invalid");
        }

    })


    pass_input2.addEventListener("change", function () {

        if (pass_input.value.localeCompare(pass_input2.value) === 0 && pass_input.value.length > 6 && pass_input.value.match(( /[0-9]/g)) && ( pass_input.value.match(/[a-z]/g) || pass_input.value.match(/[A-Z]/g) ) ){

            pass2.classList.remove("invalid");
            pass_input2.classList.remove("invalid");
            pass2.classList.add("valid");
            pass_input2.classList.add("valid");
        }

        else {
            pass2.classList.remove("valid");
            pass_input2.classList.remove("valid");
            pass2.classList.add("invalid");
            pass_input2.classList.add("invalid");
        }


        })


})